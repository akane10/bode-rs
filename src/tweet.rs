use crate::error::Error;
use crate::*;
use oauth1_request as oauth;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

type Hjson = HashMap<String, Value>;

async fn update_status(request_stat: Builder) -> Result<Hjson, Error> {
    let uri_stat = "https://api.twitter.com/1.1/statuses/update.json";
    let client = reqwest::Client::new();
    let consumer_key: String = dotenv::var("CONSUMER_KEY").expect("Missing CONSUMER_KEY");
    let consumer_secret: String = dotenv::var("CONSUMER_SECRET").expect("Missing CONSUMER_SECRET");
    let access_token: String = dotenv::var("ACCESS_TOKEN").expect("Missing ACCESS_TOKEN");
    let access_token_secret: String =
        dotenv::var("ACCESS_TOKEN_SECRET").expect("Missing ACCESS_TOKEN_SECRET");

    // Prepare your credentials.
    let token = oauth::Token::from_parts(
        consumer_key,
        consumer_secret,
        access_token,
        access_token_secret,
    );

    let authorization_header_stat = oauth::post(uri_stat, &request_stat, &token, oauth::HmacSha1);
    let uri_s = oauth::to_uri_query(uri_stat.to_owned(), &request_stat);
    let res_stat = client
        .post(&uri_s)
        .header("Authorization", authorization_header_stat.clone())
        .send()
        .await?
        .json::<HashMap<String, Value>>()
        .await?;

    // println!("{:?}", res_stat);
    Ok(res_stat)
}

#[derive(oauth::Request, Deserialize, Serialize)]
pub struct Builder {
    path: Option<String>,
    status: Option<String>,
}

impl Builder {
    pub fn new() -> Self {
        Builder {
            path: None,
            status: None,
        }
    }

    pub fn status(mut self, s: &str) -> Self {
        self.status = Some(s.to_string());
        self
    }

    pub async fn update(self) -> Result<Hjson, Error> {
        let res = update_status(self).await?;
        Ok(res)
    }
}

pub fn new() -> Builder {
    Builder::new()
}
