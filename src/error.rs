use std::fmt;

#[derive(Debug)]
pub enum Error {
    ReqwestError(reqwest::Error),
    IoError(std::io::Error),
    SerdeError(serde_json::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            Error::ReqwestError(ref x) => write!(f, "{}", x),
            Error::IoError(ref x) => write!(f, "{}", x),
            Error::SerdeError(ref x) => write!(f, "{}", x),
        }
    }
}

impl std::error::Error for Error {}

macro_rules! error_wrap {
    ($f:ty, $e:expr) => {
        impl From<$f> for Error {
            fn from(f: $f) -> Error {
                $e(f)
            }
        }
    };
}

error_wrap!(reqwest::Error, Error::ReqwestError);
error_wrap!(std::io::Error, Error::IoError);
error_wrap!(serde_json::Error, Error::SerdeError);
