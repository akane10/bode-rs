use crate::error::Error;
use reqwest;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
struct CatFactResponse {
    fact: String,
    length: i32,
}

pub async fn get_fact() -> Result<String, Error> {
    let url = "https://catfact.ninja/fact?max_length=267";
    let resp = reqwest::get(url).await?.json::<CatFactResponse>().await?;
    let text = format!("{} #catFact #cat", resp.fact);
    Ok(text)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_get_fact() {
        let fact = get_fact().await;

        assert!(fact.is_ok());
    }
}
