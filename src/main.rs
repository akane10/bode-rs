use crate::error::Error;
use dotenv;
use tokio::time;

mod cat_facts;
mod error;
mod tweet;

async fn tweet_fact() -> Result<(), Error> {
    let fact = cat_facts::get_fact().await?;
    tweet::new().status(&fact).update().await?;
    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    dotenv::dotenv().ok();
    let mut interval = time::interval(time::Duration::from_secs(25 * 3600));
    loop {
        interval.tick().await;
        match tweet_fact().await {
            Ok(_) => println!("tweeted fact"),
            Err(e) => println!("tweet fact error: {:#?}", e),
        }
    }
}
